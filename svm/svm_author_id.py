#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 2 (SVM) mini-project.

    Use a SVM to identify emails from the Enron corpus by their authors:    
    Sara has label 0
    Chris has label 1
"""
    
import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score


### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()




#########################################################
### your code goes here ###

# create the classifier
clf = SVC(C=10000,kernel="rbf")

# start timer
t0 = time()

# take a subset of the training values to make algorithm faster
features_train = features_train[:len(features_train)] 
labels_train = labels_train[:len(labels_train)] 

# fit the training data
clf.fit(features_train, labels_train)

# calculate training time
print "Time to train: ", round(time() - t0, 3), "s"

# reset timer
t0 = time()

# create prediction for test features
pred = clf.predict(features_test)

# calculate prediction time
print "Time to predict: ", round(time() - t0, 3), "s"

# check the accuracy of the predictions
accuracy = accuracy_score(pred, labels_test)

# print accuracy
print "Accuracy: ", accuracy

# 10th element
print "Prediction for 10th element: ", pred[10]

# 26th element
print "Prediction for 10th element: ", pred[26]

# 50th element
print "Prediction for 10th element: ", pred[50]

# cacluate number of predicition in test set which correspond to Chris (1)
count = 0
for x in pred:
	if x == 1:
		count += 1
print "Number of Chris emails: ", count

#print features_train
#print labels_train

#########################################################



'''
Findings:
	no. of Chris training emails: 7936
	no. of Sara training emails: 7884
	Time to train:  1.115 s
	Time to predict:  0.137 s
	Accuracy: 0.973265073948
'''
#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 1 (Naive Bayes) mini-project. 

    Use a Naive Bayes Classifier to identify emails by their authors
    
    authors and labels:
    Sara has label 0
    Chris has label 1
"""
    
import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score


### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()




#########################################################
### your code goes here ###

### create the classification
clf = GaussianNB()

### set up training time
t0 = time()

### fit the training data
clf.fit(features_train, labels_train)

## calculate training time
print "Time to train: ", round(time() - t0, 3), "s"

### set up the prediction time
t0 = time()

### create prediction for test features
pred = clf.predict(features_test)

## calculate prediction time time
print "Time to predict: ", round(time() - t0, 3), "s"

### check the accuracy of predictions
accuracy = accuracy_score(pred, labels_test)

print "Accuracy: ", accuracy
#########################################################



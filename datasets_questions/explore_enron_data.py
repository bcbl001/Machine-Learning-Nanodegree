#!/usr/bin/python

"""
    Starter code for exploring the Enron dataset (emails + finances);
    loads up the dataset (pickled dict of dicts).

    The dataset has the form:
    enron_data["LASTNAME FIRSTNAME MIDDLEINITIAL"] = { features_dict }

    {features_dict} is a dictionary of features associated with that person.
    You should explore features_dict as part of the mini-project,
    but here's an example to get you started:

    enron_data["SKILLING JEFFREY K"]["bonus"] = 5600000

"""

import pickle

enron_data = pickle.load(open("../final_project/final_project_dataset.pkl", "r"))

print "Number of people in the data set: ",len(enron_data)
print "Number of features for each person:", len(enron_data["SKILLING JEFFREY K"])

numPOI = 0
for person_name in enron_data:
    if enron_data[person_name]['poi'] == 1:
        numPOI += 1

print "People of interest: ", numPOI
print "Percent of poi: ", float(numPOI) / float(len(enron_data))

james_prentice = enron_data["PRENTICE JAMES"]
print "James Prentice: ", james_prentice

wesley_colwell = enron_data["COLWELL WESLEY"]
print "Wesley Colwell: ", wesley_colwell

jeff_skilling = enron_data["SKILLING JEFFREY K"]
print "Jeff Skilling:", jeff_skilling


lay_income = enron_data["LAY KENNETH L"]["total_payments"]
skilling_income = jeff_skilling["total_payments"]
fastow_income = enron_data["FASTOW ANDREW S"]["total_payments"]

print "Lay total payments:", lay_income
print "Skilling total payments:", skilling_income
print "Fastow total payments:", fastow_income

numEmail = 0
numSalary = 0
numTotalPayments = 0
numPoiMissingPayments = 0
for person in enron_data:
    if enron_data[person]["email_address"] != 'NaN':
        numEmail += 1
    if enron_data[person]["salary"] != 'NaN':
        numSalary += 1
    if enron_data[person]["total_payments"] == 'NaN':
        numTotalPayments += 1
    if enron_data[person]["poi"] == True and enron_data[person]["total_payments"] == 'NaN':
        numPoiMissingPayments = 0

print "Number of email addresses available:", numEmail
print "Number of salaries available:", numSalary
print "Number of total payments missing:", numTotalPayments
print "Percentage ot total payments missing:", float(numTotalPayments) / float(len(enron_data))
print "Number of POI missing payments:", numPoiMissingPayments
print "Percentage ot total payments missing:", float(numPoiMissingPayments) / float(len(enron_data))
